import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { FirebaseServiceService } from '../service/firebase-service.service';
import { LoaderService } from '../service/loader.service';
import { Router } from '@angular/router';
import { PlatformLocation, Location } from '@angular/common'

@Component({
  selector: 'app-calendar-menu',
  templateUrl: './calendar-menu.component.html',
  styleUrls: ['./calendar-menu.component.scss']
})
export class CalendarMenuComponent implements OnInit {
  
  cards: Array<{key:string, title:string, description:string, author:string, details?: {name:string}}> = [];
  calendarCategories=[];
  categoryDetails=[];
  cardsShown = false;
  cardPageIndex = 1;
  isLoading = false;
  paginations = [];

  constructor(platformLocation: PlatformLocation,protected fs:FirebaseServiceService,private loaderService:LoaderService,private router:Router, private location:Location) {
    platformLocation.onPopState(() => {
      //this.cardsShown = false;
    });
  }

  ngOnInit() {
    this.paginations = [
      {
        name:"Home",
        path:"/home",
        isBack:true
      },
      {
        name:"Calendars",
        path:"",
        isBack:false
      }
    ];
    this.cardsShown = false;
    this.cardPageIndex = 1;
    this.loaderService.toggleLoader(true);
    this.isLoading = true;
    this.fs.getCalendarCategories()
    .subscribe(calendarCategories => {
      this.calendarCategories = calendarCategories;
      this.getAllCalendarCategoryDetails(this.calendarCategories);
    },error => {
      this.isLoading = false;
      console.log(error);
    });
  }

  getAllCalendarCategoryDetails(cardCategories) {
    cardCategories.forEach(cardCategory => {
      this.getCalendarDetail(cardCategory);
    });
    
  }
x
getCalendarDetail(cardCategory:any) {
    this.fs.getCategoryDetails(cardCategory.key,"calendar_categories")
    .subscribe(categoryDetail => {
        this.categoryDetails.push(categoryDetail);
        console.log(categoryDetail);
        if (this.categoryDetails.length === this.calendarCategories.length){
          this.loaderService.toggleLoader(false);
        }
    });
     this.isLoading = false;
  }

  openCards(categoryDetail) {
    this.cardsShown = true;
    this.cardPageIndex = 2;
    this.router.navigate(['/home/calendars',categoryDetail.key]);
  }
  
  onPageClick(page) {
    if (page.isBack) {
      this.location.back();
    }
  }
}

