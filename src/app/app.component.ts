import { Component } from '@angular/core';
import * as firebase from 'firebase';
import { FirebaseServiceService } from './service/firebase-service.service';
import { slideInAnimation } from './animations';

const settings = {timestampsInSnapshots: true};
const config = {
    apiKey: "AIzaSyDSgSoYyNLqlxrldpL5BOgO0L0p_ycCzCM",
    authDomain: "srwedding-6e7c4.firebaseapp.com",
    databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
    projectId: "srwedding-6e7c4",
    storageBucket: "gs://srwedding-6e7c4.appspot.com",
    messagingSenderId: "<SENDER_ID>",
};


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    slideInAnimation
  ]
})
export class AppComponent {
  title = 'ShreeRajaGanapathyCards';

  ngOnInit() {
    firebase.initializeApp(config);
      firebase.firestore().settings(settings);
  }

  constructor(protected fs:FirebaseServiceService,) {

  }
  
}

