import { Component, OnInit, OnDestroy } from '@angular/core';
import { FirebaseServiceService } from '../service/firebase-service.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit,OnDestroy {

  feedbacks = [];
  newFeedback = [];
  constructor(private fs:FirebaseServiceService) { }

  ngOnInit() {
    this.getFeedbacks();
  }

  getFeedbacks() {
    this.feedbacks  = []; 
    this.fs.getFeedbacks().subscribe(feedbacks=>{
      this.feedbacks = feedbacks;
      console.log(feedbacks);
    },error=> {

    })
  }

  sendFeedback(form:NgForm) {
    let feedback = form.value;
    feedback.date = Date.now().toString();
    this.newFeedback.push(feedback);
    this.feedbacks.push(feedback);
    form.reset();
  }

  ngOnDestroy() {
    this.newFeedback.forEach(feedback => {
      this.fs.sendFeedback(feedback).subscribe((feedback)=>{
        //alert('Your Feedback submitted successfully!'); 
       },(error)=> {
         alert('Feedback Sending Failed!!') 
       });
    });
  }
}
