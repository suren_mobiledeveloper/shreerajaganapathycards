import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './/app-routing.module';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CardMenuComponent } from './card-menu/card-menu.component';
import { FirebaseServiceService } from './service/firebase-service.service';
import { FirebaseAuthService } from './service/firebase-auth.service';
import { LoaderService } from './service/loader.service';
import { AboutUsComponent } from './about-us/about-us.component';
import { CardDetailComponent } from './card-detail/card-detail.component';
import {NgxPageScrollModule} from 'ngx-page-scroll';
import { CardsComponent } from './card-menu/cards/cards.component';
import { DiaryMenuComponent } from './diary-menu/diary-menu.component';
import { DiariesComponent } from './diary-menu/diaries/diaries.component';
import { CalendarMenuComponent } from './calendar-menu/calendar-menu.component';
import { CalendarsComponent } from './calendar-menu/calendars/calendars.component';
import { StationaryMenuComponent } from './stationary-menu/stationary-menu.component';
import { StationariesComponent } from './stationary-menu/stationaries/stationaries.component';
import { SwiperModule } from 'angular2-useful-swiper';
import { AdsComponent } from './ads/ads.component';
import { CartComponent } from './cart/cart.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SafePipe } from '../app/service/safe';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { ThamboolamComponent } from './thamboolam/thamboolam.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactUsComponent,
    CardMenuComponent,
    AboutUsComponent,
    CardDetailComponent,
    CardsComponent,
    SafePipe,
    DiaryMenuComponent,
    DiariesComponent,
    CalendarMenuComponent,
    CalendarsComponent,
    StationaryMenuComponent,
    StationariesComponent,
    AdsComponent,
    CartComponent,
    TestimonialsComponent,
    ThamboolamComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgxPageScrollModule,
    FormsModule,
    SwiperModule
  ],
  exports: [
    SafePipe
  ],
  providers: [
    FirebaseServiceService,
    FirebaseAuthService,
    LoaderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
