import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LocalStorage } from '@ngx-pwa/local-storage';
import {Pipe,PipeTransform} from '@angular/core';
import { FirebaseServiceService } from '../service/firebase-service.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  carts = [];
  totalAmount = 0;
  phoneNumber = "";
  @ViewChild('openModal') fileInput:ElementRef;

  constructor(private localStorage:LocalStorage, private fs:FirebaseServiceService) { }

  ngOnInit() {
    this.localStorage.getItem('cart').subscribe(carts => {
      this.carts = carts;
      this.carts.forEach(cart => {
        this.totalAmount = this.totalAmount + cart.subTotal;
      });
      console.log(this.carts);
    });
    this.localStorage.getItem<String>('user').subscribe((user:any) => {
      this.phoneNumber = user;
    });
  }

  removeCartItem(cart) {
    let index = -1;
    this.carts.forEach(cart => {
      index = this.carts.indexOf(cart);
    });
    if (index != -1) {
      this.totalAmount = this.totalAmount - this.carts[index].subTotal;
      this.carts.splice(index,1);
    }
    this.localStorage.setItem('cart',this.carts).subscribe();
  }

  orderCard() {
    let orderId = "order_"+Date.now().toString();
    let order = {
      id:orderId,
      carts:this.carts,
      totalAmount:Math.round(this.totalAmount),
      phoneNumber:this.phoneNumber
    };
    this.fs.addOrder(order).subscribe(isSuccess=>{
      if (isSuccess) {
        this.carts = [];
        //alert('Your Order is created and you will get call from us shortly for processing your order.');
        this.fileInput.nativeElement.click();
        console.log('order created');
      } else {
        console.log('order failed');
      }
      this.localStorage.setItem('cart',this.carts).subscribe();
    });
  }

}
