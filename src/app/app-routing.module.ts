import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, ExtraOptions } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CardMenuComponent } from './card-menu/card-menu.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { CardDetailComponent } from './card-detail/card-detail.component';
import { CardsComponent } from './card-menu/cards/cards.component';
import { CartComponent } from './cart/cart.component';
import { AdsComponent } from './ads/ads.component';
import { DiaryMenuComponent } from './diary-menu/diary-menu.component';
import { CalendarMenuComponent } from './calendar-menu/calendar-menu.component';
import { StationaryMenuComponent } from './stationary-menu/stationary-menu.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { ThamboolamComponent } from './thamboolam/thamboolam.component';
import { DiariesComponent } from './diary-menu/diaries/diaries.component';
import { CalendarsComponent } from './calendar-menu/calendars/calendars.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent,
    children: [
      { path: 'ads', component: AdsComponent },
      { path: 'testimonials', component: TestimonialsComponent },
      { path: 'cards', component: CardMenuComponent,
        children: [
          { path: ':key', component: CardsComponent,
          children: [
            { path: 'details/:key', component: CardDetailComponent }
          ] 
        },
        ]  
      },
      { path: 'diaries', component: DiaryMenuComponent, 
        children: [
          { path: ':key', component: DiariesComponent,
          children: [
            { path: 'details/:key', component: CardDetailComponent }
          ] 
        },
        ]
      },
      { path: 'calendars', component: CalendarMenuComponent,
        children: [
          { path: ':key', component: CalendarsComponent,
          children: [
            { path: 'details/:key', component: CardDetailComponent }
          ] 
        },
        ] 
      },
      { path: 'notebooks', component: StationaryMenuComponent },
      { path: 'thamboolam', component: ThamboolamComponent },
      { path: 'aboutus', component: AboutUsComponent },
      { path: 'mycart', component: CartComponent },
      { path: '', redirectTo: 'ads', pathMatch:'full' },             
    ] 
  },
  { path: '', redirectTo: 'home', pathMatch:'full' },           
];
const routerOptions: ExtraOptions = {
  useHash: true
};

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, routerOptions)
  ], 
  exports: [ RouterModule ],
  declarations: []
})

export class AppRoutingModule { }
