import { Component, OnInit, AfterViewInit, AfterContentInit, OnChanges } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ViewChild, ElementRef} from '@angular/core';
import { HostListener } from '@angular/core';
import { FirebaseAuthService } from '../service/firebase-auth.service';
import { LoaderService } from '../service/loader.service';
import { Subscription } from 'rxjs';
import { EasingLogic } from 'ngx-page-scroll';
import { Router, RouterOutlet } from '@angular/router';
import { PhoneNumber } from '../model/PhoenNumber';
import { WindowService } from '../service/window.service';
import * as firebase from 'firebase';
import { Location } from '@angular/common';
import { GlobalService } from '../service/global.service';
import {ChangeDetectorRef} from '@angular/core'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  user:any;
  isLogin:boolean;
  sticky:number;
  toShowLoader:boolean;
  isLogined:boolean;
  subscription: Subscription;
  loginSubscription: Subscription;
  homeId = "#myHeader";
  isCartClicked = false;
  amNotARobot = false;
  windowRef: any;
  phoneNumber = new PhoneNumber()
  verificationCode: string;
  signInText = "Sign in with Phone No."
  selectedPageIndex = 0;
  signInErrorMessage = "";
  verifyClicked = false;
  @ViewChild('closeBtn') closeBtn: ElementRef;
  header:HTMLElement;
  constructor(private router:Router,private locatStorage:LocalStorage,private fireAuth:FirebaseAuthService,private loaderService:LoaderService,private win: WindowService,private location:Location,private gs:GlobalService) { 
    this.subscribeAllSubjects();
  }

  ngOnInit() {
    this.isCartClicked = false;
    this.initWindowCapcha();
    this.checkIfUserLogin();
  }

  ngOnChanges() {

  }

  initWindowCapcha() {
    this.windowRef = this.win.windowRef
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container')
    this.windowRef.recaptchaVerifier.render()
  }

  checkIfUserLogin() {
    this.locatStorage.getItem<String>('user').subscribe((user:any) => {
      this.user = user;
      console.log(this.user);
      if(!this.user) {
          this.signInText = "Sign in with Phone No."
      } else {
        this.signInText = "Signed in as "+user; 
      }
   });
   this.locatStorage.getItem<boolean>('isLogin').subscribe((isLogin) => {
     this.isLogin = isLogin;
  });  
  }
  
  subscribeAllSubjects() {
    this.subscription = this.loaderService.getMessage()
     .subscribe(toShow => { 
       this.toShowLoader = toShow; 
     });

     this.loginSubscription = this.loaderService.getLoginStatus()
     .subscribe(isLogined => { 
       this.isLogined = isLogined; 
       console.log('Login is '+this.isLogined);
      });
  }

  // ngAfterContentInit() {
  //   this.header = document.getElementById("myHeader");
  //   this.sticky = this.header.offsetTop;
  // }

  logout() {
    firebase.auth().signOut();
    this.isLogin = false;
    this.loaderService.toggleLogin(false);
    this.locatStorage.removeItem('user').subscribe(() => {});
    this.locatStorage.removeItem('isLogin').subscribe(() => {});
    this.router.navigate(['home/']);
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    //  if (window.pageYOffset >= 100) {
    //    this.header.classList.add("fixed-top");
    //  } else {
    //    this.header.classList.remove("fixed-top");
    //  }
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
    this.loginSubscription.unsubscribe();
}

 onEvent(event) {
  event.stopPropagation();
}

login(loginForm) {
  let formPhoneNumber = "91"+loginForm.value.phone_num.toString();
  console.log(formPhoneNumber);
  this.phoneNumber.country = "+"+formPhoneNumber.substring(0,2);
  this.phoneNumber.area = formPhoneNumber.substring(2,5);
  this.phoneNumber.prefix = formPhoneNumber.substring(5,8);
  this.phoneNumber.line = formPhoneNumber.substring(8,12);
  const appVerifier = this.windowRef.recaptchaVerifier;
  const num = this.phoneNumber.e164;
  console.log(this.phoneNumber);
  this.signInErrorMessage = "";

  firebase.auth().signInWithPhoneNumber(num, appVerifier)
      .then(result => {
        this.windowRef.confirmationResult = result;
        console.log('i am not a robot');
        this.amNotARobot = true;
      })
      .catch( error => { 
        console.log(error);
        this.signInErrorMessage = "Phone Number format is incorrect!";
      });
}


verifyLoginCode() {
  this.verifyClicked = true;
  this.windowRef.confirmationResult
                .confirm(this.verificationCode)
                .then( result => {
                  this.verifyClicked = false;
                  this.user = result.user.phoneNumber;
                  this.isLogin = true;
                  console.log(this.user);
                  this.locatStorage.setItem('user',this.user).subscribe();
                  this.locatStorage.setItem('isLogin', true).subscribe(() => {});
                  document.getElementById('close').click();
                  this.checkIfUserLogin();
  })
  .catch( error => {
    this.verifyClicked = false;
    this.signInErrorMessage="Incorrect code entered?"
  });
}

OnPageSelected(index,event) {
  this.selectedPageIndex = index; 
  if(index === 6) {
    this.isCartClicked = true;
     if(!this.isLogin) {
       alert('Please login to open your cart');
     } else {
      this.router.navigate(['home/mycart']);
     }
    
  } else {
    this.isCartClicked = false;
  }
}

moveToMyCart(event) {
  document.getElementById('cartLink').classList.add('active');
  this.isCartClicked = true;
  this.router.navigate(['home/cart']);
  switch (this.selectedPageIndex) {
    case 0:
      document.getElementById('link1').classList.remove('active');
      break;
      case 1:
      document.getElementById('link2').classList.remove('active');
      break;
      case 2:
      document.getElementById('link3').classList.remove('active');
      break;
      case 3:
      document.getElementById('link4').classList.remove('active');
      break;
      case 4:
      document.getElementById('link5').classList.remove('active');
      break;
      case 5:
      document.getElementById('link6').classList.remove('active');
      break;    
  
    default:
      break;
  }
}    

prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
}
}
