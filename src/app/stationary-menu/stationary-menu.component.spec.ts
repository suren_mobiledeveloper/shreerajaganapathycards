import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationaryMenuComponent } from './stationary-menu.component';

describe('StationaryMenuComponent', () => {
  let component: StationaryMenuComponent;
  let fixture: ComponentFixture<StationaryMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationaryMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationaryMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
