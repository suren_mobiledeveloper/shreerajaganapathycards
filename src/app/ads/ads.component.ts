import { Component, OnInit } from '@angular/core';
import { FirebaseServiceService } from '../service/firebase-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';''
import {SafePipe} from '../service/safe';
import { LoaderService } from '../service/loader.service';
@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.scss']
})
export class AdsComponent implements OnInit {

  config: SwiperOptions = {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    autoplay:3000,
    slidesPerGroup:1,
    slidesPerView:1,
    speed:2000,
    spaceBetween: 30
};
ads:any;
sliders=[];
right_images=[];
  constructor(protected fs:FirebaseServiceService,private router:Router,private route:ActivatedRoute,private loaderService: LoaderService) {
  }

  ngOnInit() {
    this.getAds();
  }

  getAds() {
    this.loaderService.toggleLoader(true);
    this.fs.getAds().subscribe(ads=> {
      this.ads = ads;
      this.sliders = this.ads.sliders.split(",");
      this.right_images = this.ads.right_images.split(",");
      console.log(this.sliders);
      console.log(this.right_images);
      this.loaderService.toggleLoader(false);
    },error=>{
      console.log(error);
      this.loaderService.toggleLoader(false);
    });
  }

  openCardDetail(card) {
    this.fs.selectedCard = card;
    this.router.navigate(['cards/'+"category"+'/details',card.key]);
  }
  
  getSafeUrl(url) {
  }
  
  openTestimonials() {
    this.router.navigate(['/home/testimonials']);
  }
}
