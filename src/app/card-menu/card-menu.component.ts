import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { FirebaseServiceService } from '../service/firebase-service.service';
import { LoaderService } from '../service/loader.service';
import { Router } from '@angular/router';
import { PlatformLocation, Location } from '@angular/common'

@Component({
  selector: 'app-card-menu',
  templateUrl: './card-menu.component.html',
  styleUrls: ['./card-menu.component.scss']
})
export class CardMenuComponent implements OnInit {
  cards: Array<{key:string, title:string, description:string, author:string, details?: {name:string}}> = [];
  cardCategories=[];
  categoryDetails=[];
  cardsShown = false;
  cardPageIndex = 1;
  isLoading = false;
  paginations = [];
  
  constructor(platformLocation: PlatformLocation,protected fs:FirebaseServiceService,private loaderService:LoaderService,private router:Router, private location:Location) {
    platformLocation.onPopState(() => {
      //this.cardsShown = false;
    });
  }

  ngOnInit() {
    this.paginations = [
      {
        name:"Home",
        path:"/home",
        isBack:true
      },
      {
        name:"Wedding Cards",
        path:"",
        isBack:false
      }
    ];
    this.cardsShown = false;
    this.cardPageIndex = 1;
    this.loaderService.toggleLoader(true);
    this.isLoading = true;
    this.fs.getCardCategories()
    .subscribe(cardCategories => {
      this.cardCategories = cardCategories;
      this.getAllCategoryDetails(this.cardCategories);
    },error => {
      this.isLoading = false;
      console.log(error);
    });
  }

  getAllCategoryDetails(cardCategories) {
    cardCategories.forEach(cardCategory => {
      this.getCategoryDetail(cardCategory);
    });
    
  }

  getCategoryDetail(cardCategory:any) {
    this.fs.getCategoryDetail(cardCategory.key)
    .subscribe(categoryDetail => {
        this.categoryDetails.push(categoryDetail);
        console.log(categoryDetail);
        if (this.categoryDetails.length === this.cardCategories.length){
          this.loaderService.toggleLoader(false);
        }
    });
     this.isLoading = false;
  }

  openCards(categoryDetail) {
    this.cardsShown = true;
    this.cardPageIndex = 2;
    this.router.navigate(['/home/cards',categoryDetail.key]);
  }
  
  onPageClick(page) {
    if (page.isBack) {
      //this.location.back();
      this.router.navigate([page.path]);
    }
  }
}
