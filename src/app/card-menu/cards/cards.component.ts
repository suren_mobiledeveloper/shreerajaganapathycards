import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { FirebaseServiceService } from '../../service/firebase-service.service';
import { LoaderService } from '../../service/loader.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PlatformLocation } from '@angular/common'
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {
  cards=[];
  cardDetails=[];
  category:any;
  toShowLoader = false;
  subscription: Subscription;
  paginations = [];
  cardsShown = false;

  constructor(private platformLocation:PlatformLocation,protected fs:FirebaseServiceService,private loaderService:LoaderService,private router:Router,private route:ActivatedRoute,private location: PlatformLocation) {
    this.platformLocation.onPopState(() => {
      console.log('on pop state changed');
      this.cardsShown = false;
    });
  }

  ngOnInit() {
    this.category = this.route.snapshot.paramMap.get('key');
    this.fs.selectedCategory = this.category;
    this.paginations = [
      {
        name:"Home",
        path:"/home",
        isBack:false
      },
      {
        name:"Wedding Cards",
        path:"",
        isBack:true
      },
      {
        name:this.category,
        path:"",
        isBack:false
      }
    ];
    this.subscribeAllSubjects();
    this.loaderService.toggleLoader(true);
    this.cards = [];
    this.fs.getItems("card_categories",this.category,"cards")
    .subscribe(cards => {
      this.cards = cards;
      if (this.cards.length) {
        this.getAllCardDetails(this.cards);
      } else {
        this.loaderService.toggleLoader(false);
      }
    },error => {
      console.log(error);
    });

  }

  getAllCardDetails(cards) {
    this.cardDetails = [];
    cards.forEach(card => {
      this.getItemDetail(this.category,card);
    });
  }

  getItemDetail(category,card) {
    this.fs.getItemDetail("card_categories",category,"cards",card.key)
    .subscribe(cardDetail => {
      this.cardDetails.push(cardDetail);
      if(this.cards.length === this.cardDetails.length) {
        this.loaderService.toggleLoader(false);
      }
    console.log(this.cardDetails);
    });
  }

  openCardDetail(card) {
    this.fs.selectedCard = card;
    this.cardsShown = true;
    this.router.navigate(['home/cards/'+this.category+'/details',card.key]);
  }

  subscribeAllSubjects() {
    this.subscription = this.loaderService.getMessage()
     .subscribe(toShow => { 
       this.toShowLoader = toShow; 
    });
  }

  onPageClick(page) {
    if (page.isBack) {
      this.location.back();
    } else {
      if(page.path) {
        this.router.navigate([page.path]);
      }
    }
  }
}
