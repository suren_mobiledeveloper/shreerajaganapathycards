import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FirebaseServiceService } from '../service/firebase-service.service';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { CartService } from '../service/cart.service';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-card-detail',
  templateUrl: './card-detail.component.html',
  styleUrls: ['./card-detail.component.scss']
})
export class CardDetailComponent implements OnInit {

  card:any;
  category="";
  quantities = [];
  cartItem:{card:any,quantity:number,price:number,gst:number,subTotal:number,image:string};
  images = [];
  selectedImage="";
  isLogin = false;
  paginations = [];
  gstAmount = 0;
  totalPrice = 0;
  config: SwiperOptions = {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    slidesPerGroup:3,
    slidesPerView:3,
    spaceBetween: 10
  };
  constructor(private location:Location,private router:Router,protected fs:FirebaseServiceService,private cartService:CartService,private localStorage:LocalStorage) {
    
  }

  ngOnInit() {
    this.card = this.fs.selectedCard;
    this.gstAmount = (this.card.gst/100)*this.card.price;
    this.totalPrice = Math.round(this.card.price + this.gstAmount);
    this.card.totalPrice = this.totalPrice;
    this.category = this.fs.selectedCategory;
    this.images = this.card.images.split(',');
    this.selectedImage = this.images[0];
    this.addQuantitiesArray();
    this.localStorage.getItem<boolean>('isLogin').subscribe((isLogin) => {
      this.isLogin = isLogin;
    });  
    this.paginations = [
      {
        name:"Home",
        path:"/home",
        isBack:false
      },
      {
        name:"Wedding Cards",
        path:"home/cards",
        isBack:false
      },
      {
        name:this.category,
        path:"home/cards/"+this.category,
        isBack:true
      },
      {
        name:this.card.model_name,
        path:"",
        isBack:false
      }
    ];
  }

  addQuantitiesArray() {
    for (let i = 0; i <= 1000; i+=100) {
      this.quantities.push(i);
    }
  }
  
  addToCart(cartForm:NgForm) {
    console.log(cartForm.value);
    if(this.isLogin) {
      let cartPrice = Math.round(cartForm.value.quantity*this.card.price);
      let cartGst = Math.round(cartForm.value.quantity*this.gstAmount);
      this.cartItem = { 
        card:this.card,
        quantity:cartForm.value.quantity,
        price:cartPrice,
        gst:cartGst,
        subTotal:cartPrice+cartGst,
        image:this.selectedImage
      };
      console.log(this.cartItem);
      this.cartService.addToCart(this.cartItem);  
      alert('Items added to your Cart');
    } else {
      alert('Please login to buy this item');
    }
  }

  onImageClicked(image) {
    this.selectedImage = image;
  }

  onPageClick(page) {
    if (page.isBack) {
      this.location.back();
    } else {
      if(page.path) {
        this.router.navigate([page.path]);
      }
    }
  }
}
