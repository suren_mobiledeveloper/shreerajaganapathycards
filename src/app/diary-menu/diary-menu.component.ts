import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { FirebaseServiceService } from '../service/firebase-service.service';
import { LoaderService } from '../service/loader.service';
import { Router } from '@angular/router';
import { PlatformLocation, Location } from '@angular/common'

@Component({
  selector: 'app-diary-menu',
  templateUrl: './diary-menu.component.html',
  styleUrls: ['./diary-menu.component.scss']
})
export class DiaryMenuComponent implements OnInit {
  
  cards: Array<{key:string, title:string, description:string, author:string, details?: {name:string}}> = [];
  diaryCategories=[];
  categoryDetails=[];
  cardsShown = false;
  cardPageIndex = 1;
  isLoading = false;
  paginations = [];

  constructor(platformLocation: PlatformLocation,protected fs:FirebaseServiceService,private loaderService:LoaderService,private router:Router, private location:Location) {
    platformLocation.onPopState(() => {
      //this.cardsShown = false;
    });
  }

  ngOnInit() {
    this.paginations = [
      {
        name:"Home",
        path:"/home",
        isBack:true
      },
      {
        name:"Diaries",
        path:"",
        isBack:false
      }
    ];
    this.cardsShown = false;
    this.cardPageIndex = 1;
    this.loaderService.toggleLoader(true);
    this.isLoading = true;
    this.fs.getDiaryCategories()
    .subscribe(diaryCategories => {
      this.diaryCategories = diaryCategories;
      this.getAllDiaryCategoryDetails(this.diaryCategories);
    },error => {
      this.isLoading = false;
      console.log(error);
    });
  }

  getAllDiaryCategoryDetails(cardCategories) {
    cardCategories.forEach(cardCategory => {
      this.getDiaryDetail(cardCategory);
    });
    
  }
x
  getDiaryDetail(cardCategory:any) {
    this.fs.getCategoryDetails(cardCategory.key,"diary_categories")
    .subscribe(categoryDetail => {
        this.categoryDetails.push(categoryDetail);
        console.log(categoryDetail);
        if (this.categoryDetails.length === this.diaryCategories.length){
          this.loaderService.toggleLoader(false);
        }
    });
     this.isLoading = false;
  }

  openCards(categoryDetail) {
    this.cardsShown = true;
    this.cardPageIndex = 2;
    this.router.navigate(['/home/diaries',categoryDetail.key]);
  }
  
  onPageClick(page) {
    if (page.isBack) {
      this.location.back();
    }
  }
}
