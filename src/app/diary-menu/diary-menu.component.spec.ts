import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiaryMenuComponent } from './diary-menu.component';

describe('DiaryMenuComponent', () => {
  let component: DiaryMenuComponent;
  let fixture: ComponentFixture<DiaryMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiaryMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiaryMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
