import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThamboolamComponent } from './thamboolam.component';

describe('ThamboolamComponent', () => {
  let component: ThamboolamComponent;
  let fixture: ComponentFixture<ThamboolamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThamboolamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThamboolamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
