import { Injectable } from '@angular/core';
import { LocalStorage } from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cards = [];
  constructor(private localStorage:LocalStorage) { }

  addToCart(card) {
    this.localStorage.getItem<Array<any>>('cart').subscribe((cards) => {
      if(cards && cards.length) {
        this.cards = cards;
        this.cards.push(card);
        this.localStorage.setItem('cart', this.cards).subscribe(() => {});
      } else {
        this.cards = [];
        this.cards.push(card);
        this.localStorage.setItem('cart', this.cards).subscribe(() => {});
      }
      
    });
  }
}
