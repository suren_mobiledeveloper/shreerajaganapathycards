import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  private subject = new Subject<boolean>();
  private loginSubject = new Subject<boolean>();
  
  constructor() { 
    
  }

  toggleLoader(toShow: boolean) {
    this.subject.next(toShow);
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  toggleLogin(login: boolean) {
    this.loginSubject.next(login);
  }

  getLoginStatus(): Observable<any> {
    return this.loginSubject.asObservable();
  }
}
