import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirebaseAuthService {

  constructor() {}

  signUp(email,password): Observable<any> {
    return new Observable((observer) => {
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(function(data)  {
        observer.next(data);
      })
      .catch(function(error) {
        observer.next(error);
      });
    });

  }

  login(email,password): Observable<any> {
    return new Observable((observer) => {
      firebase.auth().signInWithEmailAndPassword(email, password)
      .then(function(data)  {
        observer.next(data);
      })
      .catch(function(error) {
        observer.next(error);
      });
    });

  }

  
}
