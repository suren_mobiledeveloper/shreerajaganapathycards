import { Injectable } from '@angular/core';
import { FirebaseServiceService } from './firebase-service.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  cardPageIndex = 1;
  cardCategories = [];
  categoryDetails = [];

  constructor(private fs:FirebaseServiceService) { 
    this.fs.getCardCategories()
  .subscribe(cardCategories => {
    this.cardCategories = cardCategories;
    //console.log(this.cardCategories);
    this.getAllCategoryDetails(this.cardCategories);
  },error => {
    console.log(error);
  });
  }

  getAllCategoryDetails(cardCategories) {
    console.log(cardCategories);
    cardCategories.forEach(cardCategory => {
      this.getCategoryDetail(cardCategory);
    });
    
  }
  
  getCategoryDetail(cardCategory:any) {
    this.fs.getCategoryDetail(cardCategory.key)
    .subscribe(categoryDetail => {
        this.categoryDetails.push(categoryDetail);
    });
  }
}
