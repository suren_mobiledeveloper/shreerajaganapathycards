import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class FirebaseServiceService {
  
  cards = [];
  cardCategories=[];
  diaries = [];
  diaryCategories=[];
  calendars = [];
  calendarCategories=[];
  stationaries = [];
  stationaryCategories=[];
  selectedCard:any;
  selectedCategory:any;
  ads:any;
  feedbacks = [];
  constructor( private sanitizer: DomSanitizer) { }

  getAds(): Observable<any> {
    let ref = firebase.firestore().collection('ads').doc("images");
    return new Observable((observer) => {
      ref.get().then((doc) => {
        let data = doc.data();
         observer.next({
          key: doc.id,
          right_images:data.right_images,
          sliders:data.sliders
        });
      });
    });
  }

  getCardCategories(): Observable<any> {
    let ref = firebase.firestore().collection('card_categories');
    if (!this.cardCategories.length) {
        return new Observable((observer) => {
        ref.onSnapshot((querySnapshot) => {
        this.cardCategories = [];
        querySnapshot.forEach((doc) => {
          let data = doc.data();
          this.cardCategories.push({
            key: doc.id
          });
        });
        observer.next(this.cardCategories);
      });
    });
    } else {
      return new Observable((observer)=>{
        observer.next(this.cardCategories);
      });
    }
  }

  getCards(category): Observable<any> {
    let cards = [];
      let ref = firebase.firestore().collection('card_categories').doc(category).collection('cards');
      console.log(ref);
      return new Observable((observer) => {
          ref.onSnapshot((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            let data = doc.data();
            cards.push({
              key: doc.id
            });
          });
          observer.next(cards);
        });
      });
  }

  getItems(category,categoryId,subCategory): Observable<any> {
    let items = [];
  
      let ref = firebase.firestore().collection(category).doc(categoryId).collection(subCategory);
      console.log(ref);
      return new Observable((observer) => {
          ref.onSnapshot((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            let data = doc.data();
            items.push({
              key: doc.id
            });
          });
          observer.next(items);
        });
      });
    
    
  }

  getDiaryCategories(): Observable<any> {
    let ref = firebase.firestore().collection('diary_categories');
    if (!this.diaryCategories.length) {
        return new Observable((observer) => {
        ref.onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          let data = doc.data();
          this.diaryCategories.push({
            key: doc.id
          });
        });
        observer.next(this.diaryCategories);
      });
    });
    } else {
      return new Observable((observer)=>{
        observer.next(this.diaryCategories);
      });
    }
  }

  getDiaries(category): Observable<any> {
    if(!this.diaries.length) {
      let ref = firebase.firestore().collection('diary_categories').doc(category).collection('diaries');
      return new Observable((observer) => {
          ref.onSnapshot((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            let data = doc.data();
            this.diaries.push({
              key: doc.id
            });
          });
          observer.next(this.diaries);
        });
      });
    } else {
      return new Observable((observer)=>{
        observer.next(this.diaries);
      });
    }
  }

  getCalendarCategories(): Observable<any> {
    let ref = firebase.firestore().collection('calendar_categories');
    if (!this.calendarCategories.length) {
        return new Observable((observer) => {
        ref.onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          let data = doc.data();
          this.calendarCategories.push({
            key: doc.id
          });
        });
        observer.next(this.calendarCategories);
      });
    });
    } else {
      return new Observable((observer)=>{
         observer.next(this.calendarCategories);
      });
    }
  }

  getCalendars(category): Observable<any> {
    if(!this.calendars.length) {
      let ref = firebase.firestore().collection('calendar_categories').doc(category).collection('calendars');
      return new Observable((observer) => {
          ref.onSnapshot((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            let data = doc.data();
            this.calendars.push({
              key: doc.id
            });
          });
          observer.next(this.calendars);
        });
      });
    } else {
      return new Observable((observer)=>{
        observer.next(this.calendars);
      });
    }
  }

  getStationaryCategories(): Observable<any> {
    let ref = firebase.firestore().collection('stationary_categories');
    if (!this.stationaryCategories.length) {
        return new Observable((observer) => {
        ref.onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          let data = doc.data();
          this.stationaryCategories.push({
            key: doc.id
          });
        });
        observer.next(this.stationaryCategories);
      });
    });
    } else {
      return new Observable((observer)=>{
        observer.next(this.stationaryCategories);
      });
    }
  }

  getStationaries(category): Observable<any> {
    if(!this.stationaries.length) {
      let ref = firebase.firestore().collection('stationary_categories').doc(category).collection('stationaries');
      return new Observable((observer) => {
          ref.onSnapshot((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            let data = doc.data();
            this.stationaries.push({
              key: doc.id
            });
          });
          observer.next(this.stationaries);
        });
      });
    } else {
      return new Observable((observer)=>{
        observer.next(this.stationaries);
      });
    }
  }

  getCardDetail(category,key: string): Observable<any> {
    let ref = firebase.firestore().collection('card_categories').doc(category).collection('cards').doc(key);
    return new Observable((observer) => {
      ref.get().then((doc) => {
        let data = doc.data();
         observer.next({
          key: doc.id,
          model_name:data.model_name,
          price:data.price,
          color:data.color,
          images:data.images,
          courier_charges:data.courier_charges,
          printing_charges:data.printing_charges
        });
      });
    });
  }

  getItemDetail(category,categoryId,subCategory,subCategoryId): Observable<any> {
    let ref = firebase.firestore().collection(category).doc(categoryId).collection(subCategory).doc(subCategoryId);
    return new Observable((observer) => {
      ref.get().then((doc) => {
        let data = doc.data();
         observer.next({
          key: doc.id,
          model_name:data.model_name,
          price:data.price,
          color:data.color,
          images:data.images,
          courier_charges:data.courier_charges,
          printing_charges:data.printing_charges,
          gst:data.gst
        });
      });
    });
  }

  getDiaryDetail(category,key: string): Observable<any> {
    let ref = firebase.firestore().collection('diary_categories').doc(category).collection('diaries').doc(key);
    return new Observable((observer) => {
      ref.get().then((doc) => {
        let data = doc.data();
         observer.next({
          key: doc.id,
          model_name:data.model_name,
          price:data.price,
          color:data.color,
          courier_charges:data.courier_charges,
          printing_charges:data.printing_charges
        });
      });
    });
  }

  getCalendarDetail(category,key: string): Observable<any> {
    let ref = firebase.firestore().collection('calendar_categories').doc(category).collection('calendars').doc(key);
    return new Observable((observer) => {
      ref.get().then((doc) => {
        let data = doc.data();
         observer.next({
          key: doc.id,
          model_name:data.model_name,
          price:data.price,
          color:data.color,
          courier_charges:data.courier_charges,
          printing_charges:data.printing_charges
        });
      });
    });
  }

  getStationaryDetail(category,key: string): Observable<any> {
    let ref = firebase.firestore().collection('stationary_categories').doc(category).collection('stationaries').doc(key);
    return new Observable((observer) => {
      ref.get().then((doc) => {
        let data = doc.data();
         observer.next({
          key: doc.id,
          model_name:data.model_name,
          price:data.price,
          color:data.color,
          courier_charges:data.courier_charges,
          printing_charges:data.printing_charges
        });
      });
    });
  }


  getCategoryDetail(key: string): Observable<any> {
    let ref = firebase.firestore().collection('card_categories').doc(key);
    return new Observable((observer) => {
      ref.get().then((doc) => {
        let data = doc.data();
         observer.next({
          key: doc.id,
          name:data.name,
          image:data.image
        });
      });
    });
  }

  getCategoryDetails(key: string,collection_name:string): Observable<any> {
    let ref = firebase.firestore().collection(collection_name).doc(key);
    return new Observable((observer) => {
      ref.get().then((doc) => {
        let data = doc.data();
         observer.next({
          key: doc.id,
          name:data.name,
          image:data.image
        });
      });
    });
  }


  addOrder(order): Observable<any> {
    let ref = firebase.firestore().collection('orders').doc(order.id);
    let isSuccess = false;
    return new Observable((observer) => {
      ref.set(order).then((order) => {
         isSuccess = true;
         observer.next(isSuccess);
      },(error)=>{
        isSuccess = false;
        observer.next(isSuccess);
      });
    });
  }

  sendFeedback(feedback): Observable<any> {
    let ref = firebase.firestore().collection('feedbacks').doc(Math.random().toString());
    let isSuccess = false;
    return new Observable((observer) => {
      ref.set(feedback).then((feedback) => {
         isSuccess = true;
         observer.next(isSuccess);
      },(error)=>{
        isSuccess = false;
        observer.next(isSuccess);
      });
    });
  }

  getFeedbacks(): Observable<any> {     
   let feedbacks=[]; 
    let ref = firebase.firestore().collection('feedbacks').orderBy("date");
      return new Observable((observer) => {
          ref.onSnapshot((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            let data = doc.data();
            feedbacks.push({
              key: doc.id,
              name:data.name,
              feedback:data.feedback
            });
          });
          observer.next(feedbacks);
          console.log(feedbacks);
        });
      });
  }
}

